# Parallax Hero Readme #

A really cool parallax hero section.

### What is this repository for? ###

I created this to try creating parallax scroll effects on elements on the page.

### How do I get set up? ###

Download the repo and drag and drop into your web browser. All the code is in the html file.

Libraries used: JQuery (CDN).

### Coming Soon ###

Put CSS and JS in seperate files.

### Contribution guidelines ###

Anyone can contribute. Just explain why the change is necessary.

### Who do I talk to? ###

Veronica Rivera
Website [justvcreative.com](http://justvcreative.com)
Twitter [@justvcreative](https://twitter.com/justvcreative)
Instagram [@justvcreative](https://instagram.com/justvcreative)
Facebook [justvcreative](https://www.facebook.com/justvcreative)